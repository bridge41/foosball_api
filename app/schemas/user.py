from typing import Optional, List
from pydantic import BaseModel, UUID4

class UserBase(BaseModel):
    id: Optional[int]
    first_name: Optional[str]
    last_name: Optional[str]

class User(UserBase):
    id: Optional[int]
    first_name: Optional[str]
    last_name: Optional[str]
    nickname: Optional[str]

class UpdateUser(UserBase):
    first_name: str
    last_name: str
    nickname: Optional[str]
    password: str
