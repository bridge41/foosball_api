from datetime import datetime
from typing import Optional, List
from pydantic import BaseModel, UUID4


class GameBase(BaseModel):
    id: Optional[UUID4]
    order_number: Optional[int]
    start_time: Optional[datetime] = datetime.now()
    end_time: Optional[datetime]
    time_limit: Optional[int]
    match_id: Optional[str]


class Game(GameBase):
    id: UUID4
    order_number: int