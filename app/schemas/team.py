from typing import Optional, List
from pydantic import BaseModel, UUID4
from app.schemas.user import User


class TeamBase(BaseModel):
    id: Optional[UUID4]
    name: Optional[str]


class Team(TeamBase):
    id: UUID4
    users: List[User] = []