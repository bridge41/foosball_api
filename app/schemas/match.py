from typing import Optional, List
from pydantic import BaseModel, UUID4
from datetime import datetime
from team import Team
from game import Game


class MatchBase(BaseModel):
    id: Optional[UUID4]
    start_time: Optional[datetime] = datetime.now()
    end_time: Optional[datetime]
    time_limit: Optional[int]
    winner: Optional[Team]
    games: Optional[Game]


class Match(MatchBase):
    id: UUID4