from datetime import datetime
from uuid import uuid4
from sqlalchemy import Table, Column, String, Integer, DateTime
from sqlalchemy.dialects.postgresql import UUID
from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.schema import ForeignKey
from sqlalchemy.sql.sqltypes import Boolean

from app.db import Base

team_user = Table(
    "team_user",
    Base.metadata,
    Column("team_id", UUID(as_uuid=True), ForeignKey("teams.id"), primary_key=True),
    Column("user_id", Integer, ForeignKey("users.id"), primary_key=True),
)


class UserModel(Base):
    __tablename__ = "users"

    # id = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)
    id = Column(Integer, primary_key=True, autoincrement=True)
    first_name = Column(String)
    last_name = Column(String)
    nickname = Column(String)
    rfid = Column(String, unique=True)

    _teams = relationship("TeamModel", secondary=team_user, backref=backref("_users"))


class Game_Team(Base):
    __tablename__ = "game_teams"

    game_id = Column(UUID(as_uuid=True), ForeignKey("games.id"), primary_key=True)
    team_id = Column(UUID(as_uuid=True), ForeignKey("teams.id"), primary_key=True)
    score = Column(Integer, default=0)
    is_winner = Column(Boolean, default=False)

    _game = relationship("GameModel", backref=backref("_teams", cascade="delete, delete-orphan"))
    _team = relationship("TeamModel", backref=backref("_games", cascade="delete, delete-orphan"))


class TeamModel(Base):
    __tablename__ = "teams"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)
    name = Column(String)


class GameModel(Base):
    __tablename__ = "games"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)
    order_number = Column(Integer)
    start_time = Column(DateTime, default=datetime.now())
    end_time = Column(DateTime)
    time_limit = Column(Integer)
    match_id = Column(UUID(), ForeignKey("matches.id"))

    _match = relationship("MatchModel", backref=backref("_games"))


class MatchModel(Base):
    __tablename__ = "matches"

    id = Column(UUID(as_uuid=True), primary_key=True, index=True, default=uuid4)
    start_time = Column(DateTime, default=datetime.now())
    end_time = Column(DateTime)
    time_limit = Column(Integer)
    winner = Column(UUID(), ForeignKey("teams.id"))
