from typing import List
from sqlalchemy.dialects.postgresql.base import UUID
from sqlalchemy.orm import Session, join
from sqlalchemy.orm.util import outerjoin
from app.models import TeamModel, UserModel
from app.schemas.team import Team
from app.schemas.user import User


def get_all(db: Session) -> List[Team]:
    try:
        rows = db.query(TeamModel).all()
        teams: List[Team] = []
        for row in rows:
            team = Team(**row.__dict__)
            for db_user in team._users:
                team.users.append(User(**db_user.__dict__))
            teams.append(team)
        return teams

    except Exception as e:
        raise e


def get_by_id(db: Session, id: UUID()) -> Team:
    try:
        row = db.query(TeamModel).get(id)
        team = Team(**row.__dict__)
        for db_user in team._users:
            team.users.append(User(**db_user.__dict__))
        return team
    except Exception as e:
        raise e


def get_by_user_ids(db: Session, id1: str, id2: str):
    row = (
        db.query(TeamModel)
        .select_from(join(TeamModel, UserModel, TeamModel._users))
        .filter(UserModel.id.in_([id1, id2]))
        .all()
    )
    if row == None:
        db_user1 = db.query(UserModel).get(id1)
        db_user2 = db.query(UserModel).get(id2)
        name = f'{db_user1.id}_{db_user2.id}'
        new_team = TeamModel(
            name=name,
            _users=[db_user1, db_user2]
        )
        db.add(new_team)
        db.commit()
        row = (
            db.query(TeamModel)
            .select_from(join(TeamModel, UserModel, TeamModel._users))
            .filter(UserModel.id.in_([id1,id2]))
            .one_or_none()
        )
    team = Team(**row.__dict__)
    for user in row._users:
        team.users.append(User(**user.__dict__))
    return team