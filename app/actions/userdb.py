from typing import List
from sqlalchemy.dialects.postgresql.base import UUID
from sqlalchemy.orm import Session
from app.models import UserModel
from app.schemas.user import User


def get_all(db: Session) -> List[User]:
    try:
        rows = (
            db.query(UserModel)
            .with_entities(UserModel.id, UserModel.first_name, UserModel.last_name, UserModel.teams)
            .all()
        )
        users: List[User] = []
        for row in rows:
            user = User(**row.__dict__)
            users.append(user)
        return users
    except Exception as e:
        raise e


def get_by_id(db: Session, id: UUID()) -> User:
    try:
        row = (
            db.query(UserModel)
            .with_entities(UserModel.id, UserModel.first_name, UserModel.last_name, UserModel.teams)
            .get(id)
        )
        user = User(**row.__dict__)
        return user
    except Exception as e:
        raise e


def get_by_rfid(db: Session, rfid: str) -> User:
    try:
        row = db.query(UserModel).filter_by(rfid=rfid).one_or_none()
        if row == None:
            db.add(UserModel(rfid=rfid))
            db.commit()
            row = db.query(UserModel).filter_by(rfid=rfid).one_or_none()
        new_user = User(**row.__dict__)
        return new_user
    except:
        print("error in add db")
