from fastapi import APIRouter, Depends, HTTPException
from typing import List, Any
from sqlalchemy.orm import Session
from app.dependencies import get_db

from app.schemas.user import User
from app.actions import userdb
from pydantic import UUID4


router = APIRouter(prefix="/user", tags=["user"])

@router.post("/get", response_model=User)
def create_user(rfid: str, db: Session = Depends(get_db)):
    user: User = userdb.get_by_rfid(db, rfid)
    return user