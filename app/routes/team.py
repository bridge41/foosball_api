from fastapi import APIRouter, Depends, HTTPException
from typing import List, Any
from sqlalchemy.orm import Session
from app.dependencies import get_db

from app.schemas.team import Team
from app.actions import userdb, teamdb
from pydantic import UUID4


router = APIRouter(prefix="/team", tags=["team"])


@router.post("/get", response_model=Team)
def create_user(id1: str, id2: str, db: Session = Depends(get_db)):
    team: Team = teamdb.get_by_user_ids(db, id1, id2)
    return team
